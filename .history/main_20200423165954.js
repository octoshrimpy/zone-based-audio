//grab audio
var audio1 = document.querySelector("._1 audio");
var audio2 = document.querySelector("._2 audio");
var audio3 = document.querySelector("._3 audio");
var audio4 = document.querySelector("._4 audio");

//aggregate
var audios = [
    audio1,
    audio2,
    audio3,
    audio4
];

//initial mute
audios.forEach(function(audio, index){
    audio.volume = 0;
});


//setup cursor grabber
var cursorX;
var cursorY;

document.onmousemove = function(e){
    cursorX = e.pageX;
    cursorY = e.pageY;
}

//helper funcs
function calculateDistance(elem, mouseX, mouseY) {
    return Math.abs( Math.floor(
        Math.sqrt(
            Math.pow(mouseX - (elem.offsetLeft+(elem.offsetWidth/2)), 2) +
            Math.pow(mouseY - (elem.offsetTop+(elem.offsetHeight/2)), 2)
        )
    ) - Math.round(elem.offsetWidth/2) );
}


//setup gameloop
function loop(timestamp) {
    audios.forEach(function(audio){
        //grab parent for zone
        var parent = audio.parentElement;
        var audioName = parent.classList[0];
        //calc distance
        var distance = calculateDistance(parent, cursorX, cursorY);
        
        //set volume
        //needs to be percentages, not hardcoding like this. 
        var volume = 190 - distance;
        
        //NaN if mouse is outside screen boundaries
        if(isNaN(volume) || volume < 0) {
            volume = 0;
        } else {
            //html5 audio volume is 0 to 1
            volume = volume/100;
            //awful fix
            //causes full volume pretty far away from center
            if(volume > 1) {
                volume = 1;
            }
        }
        //pretty self-explanatory
        // console.log(audioName, volume)
        audio.volume = volume;
        
    })
}

//init gameloop
window.setInterval(loop, 100)