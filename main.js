
var audios, audio1, audio2, audio3, audio4;
var cursorX, cursorY;
var firstMove;

function grabAudios(){
    //grab audio
    audio1 = document.querySelector("._1 audio");
    audio2 = document.querySelector("._2 audio");
    audio3 = document.querySelector("._3 audio");
    audio4 = document.querySelector("._4 audio");
    
    //aggregate
    audios = [
        audio1,
        audio2,
        audio3,
        audio4
    ];
}

function muteAudios() {
    //initial mute
    audios.forEach(function(audio, index){
        audio.volume = 0;
    });
}

//helper functions
function calculateDistance(elm, mX, mY) {
    elmCenterX = (window.scrollX + elm.getBoundingClientRect().left) + (elm.offsetWidth / 2)
    elmCenterY = (window.scrollY + elm.getBoundingClientRect().top) + (elm.offsetHeight / 2)

    return Math.floor(
        Math.sqrt(
            Math.pow(mX - elmCenterX, 2) +
            Math.pow(mY - elmCenterY, 2) 
        )
    )
}


//setup gameloop
function loop(timestamp) {
    audios.forEach(function(audio){

        //grab parent for zone
        var parent = audio.parentElement;
        var audioName = parent.classList[0];
        var circle = parent.querySelector(".circle");

        //calc distance
        var distance = calculateDistance(circle, cursorX, cursorY);
        var elmEdgeDistance = circle.offsetWidth / 2; //assuming it's a circle. 

        // percent based on size of area
        var volume = (distance * 100) / elmEdgeDistance;

        
        // html5 audio is between 0 and 1
        var volume = volume / 100;

        // invert volume, since distance % is 100 at edge and 0 at center but we want inverse for volume
        volume = 1 - volume;

        // cap volume
        if(isNaN(volume) || volume < 0) {
            volume = 0;
        }

        //pretty self-explanatory
        audio.volume = volume;
        console.log(`distance to ${audioName}: ${distance}; volume: ${volume}`); 
    })
}

function startAudio() {   
    audios.forEach(function(audio){
        audio.play();
    });
    document.querySelector('.explanation').remove();
}

//init gameloop
window.onload = () => {
    
    grabAudios();

    muteAudios();

    //setup cursor grabber
    document.onmousemove = function(e){
        cursorX = e.pageX;
        cursorY = e.pageY;
    }

    //start game
    window.setInterval(loop, 100)
}

